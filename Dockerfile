FROM maven:3.6.1-jdk-8

EXPOSE 8080
WORKDIR /app

COPY src/ pom.xml ./
RUN mvn clean install

ENTRYPOINT mvn spring-boot:run
